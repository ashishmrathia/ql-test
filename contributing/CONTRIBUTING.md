# Contributing to Project #
We love your input! We want to make contributing to this project as easy and transparent as possible, whether it's:

1) Reporting a bug
2) Discussing the current state of the code
3) Submitting a fix
4) Proposing new features
5) Becoming a maintainer


### We Develop with Github ###
We use github to host code, to track issues and feature requests, as well as accept pull requests.

### We Use SkullCandy Flow, So All Code Changes Happen Through Pull Requests ###
Pull requests are the best way to propose changes to the codebase (we use Github Flow). We actively welcome your pull requests:

1) Fork the repo and create your branch from master.
2) If you've added code that should be tested, add tests.
3) If you've changed APIs, update the documentation.
4) Ensure the test suite passes.
5) Make sure your code lints.
6) Issue that pull request!

### Report bugs using JIRA ###
Some info here about your project JIRA template.

### Write bug reports with detail, background, and sample code
This is an example of a bug report I wrote, and I think it's not a bad model. Here's another example from Craig Hockenberry, an app developer whom I greatly respect.

##### Great Bug Reports tend to have:

A quick summary and/or background
Steps to reproduce
Be specific!
Give sample code if you can. 
What you expected would happen
What actually happens
Notes (possibly including why you think this might be happening, or stuff you tried that didn't work)
People love thorough bug reports.

### Use a Consistent Coding Style
For eg:
2 spaces for indentation rather than tabs
You can try running npm run lint for style unification....etc 

### License
if Any.....

### References
If Any.....